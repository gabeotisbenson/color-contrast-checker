import Main from '~/views/Main.vue';
import Vue from 'vue';
import 'modern-normalize';

export default new Vue({
	el: 'main',
	render: h => h(Main)
});
